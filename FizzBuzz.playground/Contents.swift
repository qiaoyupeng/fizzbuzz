//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

for number in 1...100
{
    var i = number % 3
    var j = number % 5
    
    if i != 0 && j != 0
    {
        print("\(number)")
        continue
    }
    
    if i == 0 && j == 0
    {
        print( "Fizz and Buzz" )
        continue
    }
    else if  i == 0
    {
        print("Fizz")
        continue
    }
    else
    {
        print("Buzz")
        continue
    }
        
    
}
